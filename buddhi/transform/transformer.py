from psf.jivrtt.flow.base import BaseTransformer

from buddhi.model.bollinger import TABollingerBandsModel


class ModelTransformer(BaseTransformer):

    def transform(self, ts_dfs):
        result_dfs = []
        model = TABollingerBandsModel()
        for ts_df in ts_dfs:
            result_dfs.append(model.apply(ts_df))
        return result_dfs
