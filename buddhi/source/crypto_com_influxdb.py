from influx.client import InfluxQLClient
from psf.jivrtt.flow.base import BaseSource


class CryptoComInfluxDBSource(BaseSource):

    def __init__(self):
        self._client = InfluxQLClient()

    def inputs(self):
        """ Gets the inputs to be processed by models """
        pass

    def read(self, source_ids=None):
        pass
