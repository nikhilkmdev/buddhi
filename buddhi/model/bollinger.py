from logging import info, debug

from buddhi.core.base import BaseModel


class TABollingerBandsModel(BaseModel):

    def __init__(self, std_dev=2, days_length=20, measure_col='prc'):
        """
        Uses Pandas TA library API for calculating BB bands around the mean
        :param std_dev: the standard deviation
        :param days_length: the length of samples for analysis
        """
        super().__init__(std_dev=std_dev, days_length=days_length)
        self._std_dev = std_dev
        self._days_length = days_length
        self._measure_col = measure_col

    def apply(self, ts):
        """ Applies the Bollinger bands model to the time series to find possible trades
        :param: ts: the time series to be analyzed
        :return: list of possible trades
        """
        import pandas_ta as ta
        info(ta)
        ts = ts.rename(columns={self._measure_col: 'close'})
        ts.ta.bbands(append=True, stdev=self._std_dev, length=self._days_length)
        info(f'Applied bollinger band model successfully on ts. Std dev: {self._std_dev} & Length: {self._days_length}')
        debug(f'{ts.tail()}')
        return self._find_possible_trades(ts)

    def apply_history(self):
        """
        Applies the model to history and finds probable trades and comes up with a PnL
        :return: opportunities for analysis
        """
        pass

    def _find_possible_trades(self, ts):
        lower_band = f'BBL_{self._days_length}'
        mean_band = f'BBM_{self._days_length}'
        upper_band = f'BBU_{self._days_length}'
        long_opportunities = ts[ts[lower_band] >= ts['close']]
        # short_opportunities = ts[ts['close'] < ts[upper_band]]
        debug(self._get_long_trades(long_opportunities, lower_band, mean_band))
        return long_opportunities

    @staticmethod
    def _get_long_trades(long_opportunities, lower_band_col, mean_band_col):
        info(f'Analyzing {len(long_opportunities)} opportunities for long trade')
        for row_index in range(len(long_opportunities)):
            row = long_opportunities.iloc[row_index]
            info(f"Opportunity: {row['DATE']} - BUY AT - {row[lower_band_col]} - SELL AT - {row[mean_band_col]}")
