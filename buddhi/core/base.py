from abc import abstractmethod
from enum import Enum


class BaseModel(object):

    def __init__(self, **model_kwargs):
        """
        Base class for all models to abide by
        :param model_kwargs: the model keyword args
        """
        self._kwargs = model_kwargs

    @abstractmethod
    def apply(self, ts):
        """
        Apply the model to the given time series
        :param ts: the time series to be analyzed
        :return: leads for trades
        """

    @abstractmethod
    def apply_history(self):
        """
        Applies the model to history and finds probable trades and comes up with a PnL
        :return: opportunities for analysis
        """


class Opportunity(object):
    pass


class Side(Enum):
    BUY = 'BUY'
    SELL = 'SELL'
