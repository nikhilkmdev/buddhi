# -*- coding: utf-8 -*-
from distutils.core import setup

from setuptools import find_packages

setup(
    name='py-vanij-buddhi',
    packages=find_packages(exclude=('tests',)),
    version='2020.05.31',
    description='Module responsible for analyzing the time series and generating leads for trades',
    long_description='Module responsible for sourcing time series from data sources',
    long_description_content_type="text/markdown",
    author='Nikhil K Madhusudhan (nikhilkmdev)',
    author_email='nikhilkmdev@gmail.com',
    maintainer='Nikhil K Madhusudhan (nikhilkmdev)',
    maintainer_email='nikhilkmdev@gmail.com',
    install_requires=['py-simple-flow', 'pandas', 'pyyaml', 'pandas-ta', 'py-influxdb', 'dash==1.15.0'],
    keywords=['timeseries', 'history', 'python3'],
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
)
